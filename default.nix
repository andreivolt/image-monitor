with import <nixpkgs> {}; let
	www = stdenv.mkDerivation rec {
		name = "www";
		
		src = builtins.filterSource (name: type: !(lib.hasPrefix (toString ./.git) name)) ./.;
		
		nativeBuildInputs = with pkgs; [
			ipfs
			ruby
		];

		buildPhase = ''
			export IPFS_PATH="$TMP/ipfs"
			ipfs init

			./build.rb
		'';
		
		installPhase = ''
			mv build "$out"
		'';
	};

	stunnel-config = writeText "stunnel.conf" ''
		[https]
		client = yes
		accept = 127.0.0.1:8888
		connect = ipfs.infura.io:5001
		verifyChain = yes
		CAfile = ${cacert}/etc/ssl/certs/ca-bundle.crt
		checkHost = ipfs.infura.io
		OCSPaia = yes
	'';
in stdenv.mkDerivation rec {
	name = "upload";
	
	src = builtins.filterSource (name: type: !(lib.hasPrefix (toString ./.git) name)) ./.;
	
	nativeBuildInputs = with pkgs; [
		ipfs
		stunnel
	];

	buildPhase = ''
		stunnel ${stunnel-config}

		export IPFS_PATH="$TMP/ipfs"
		ipfs init

		hash=$(./ipfs-add.sh --api=/dns/127.0.0.1/tcp/8888 -rQ ${www})

		echo "WWW_HASH=$hash" >env
		echo "WWW_HASH32=$(ipfs cid base32 $hash)" >>env
	'';
	
	installPhase = ''
		mkdir "$out"
		mv env "$out"
		ln -s ${www} "$out/www"
	'';
}
