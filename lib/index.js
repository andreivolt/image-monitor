addEventListener("hashchange", _ => location.reload());

let add = document.getElementById("add");

add.addEventListener("click", e => {
	edit(newImage());
})

let hash = document.location.hash.replace(/^#/, "");
hash.split("/").forEach(buildImage);

function newImage() {
	add.insertAdjacentHTML("beforebegin", "<a><img></a>");
	return add.previousSibling.firstChild;
}

function buildImage(config) {
	config = new URLSearchParams(config);
	let u = config.get("u") || config.get("url");

	let img = newImage();
	changeInterval(img, config.get("s"));
	changeUrl(img, u);
}

let updateCount = 0;

function changeInterval(img, sec) {
	sec = sec || 1;

	if (+img.dataset.s == sec) return;

	if (img.dataset.interval) clearInterval(img.dataset.interval);

	img.dataset.s = sec;
	img.dataset.interval = setInterval(_ => {
		img.src = img.parentNode.href + "#" + updateCount++;
	}, img.dataset.s * 1000);
}

function changeUrl(img, url) {
	img.src = url;
	img.parentNode.href = url;
}

function updateLocation() {
	document.location.hash = [...document.querySelectorAll("img")]
		.map(el => {
			let r = new URLSearchParams;

			r.append("u", el.parentNode.href);

			let s = el.dataset.s;
			if (s != 1) {
				r.append("s", s);
			}

			return r;
		})
		.join("/");
}

function edit(img) {
	document.body.insertAdjacentHTML("beforeend", `<form>
		<label>Image: <input name=u></label>
		<label>Seconds: <input type=number name=s></label>
		<button type=submit>Update</button>
	</form>`);
	let f = document.body.lastChild;
	f.s.value = img.dataset.s;
	f.u.value = img.parentNode.href;

	f.addEventListener("submit", e => {
		e.preventDefault();
		changeInterval(img, +f.s.value);
		changeUrl(img, f.u.value);

		f.remove();

		updateLocation();
	})
}

document.addEventListener("click", e => {
	let el = e.target;
	if (el.nodeName != "IMG") return;
	e.preventDefault();

	edit(el);
})
